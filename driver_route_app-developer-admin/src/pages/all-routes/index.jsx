import React, { useMemo } from "react";
import {
  Box,
  Button,
  Typography,
  useTheme,
  CircularProgress,
} from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import { tokens } from "../../theme";
import { mockDataTeam } from "../../data/mockData";
import CustomDialogForMap from "../../components/common/dialog/CustomDialogForMap";
import Header from "../../components/Header";
import {
  useGetRouteByDateAndUserMutation,
  useGetAllDriversWithRoutesQuery,
} from "../../redux/services/route/route.service";
import { LoadingButton } from "@mui/lab";
import toast from "react-hot-toast";

const AllRoutes = () => {
  const theme = useTheme();
  const [openMap, setOpenMap] = React.useState(false);
  const [routeMap, setRouteMap] = React.useState(null);
  const [rowData, setRowData] = React.useState(null);
  const { data, isFetching } = useGetAllDriversWithRoutesQuery();
  const routeList = useMemo(() => {
    let tempRouteList = [];
    if (data?.data) {
      for (let i = 0; i < data?.data?.length; i++) {
        for (let j = 0; j < data?.data[i]?.route_date?.length; j++) {
          tempRouteList.push({
            id: Math.floor(Math.random() * 1000),
            user_id: data?.data[i]?.id,
            first_name: data?.data[i]?.first_name,
            last_name: data?.data[i]?.last_name,
            email: data?.data[i]?.email,
            route_date: data?.data[i]?.route_date[j],
          });
        }
      }
      return tempRouteList;
    }
  }, [data]);
  const colors = tokens(theme.palette.mode);
  const [getRouteByDateAndUser, { isLoading: isMapLoading }] =
    useGetRouteByDateAndUserMutation();
  const columns = [
    { field: "id", headerName: "Id" },
    {
      field: "first_name",
      headerName: "First Name",
      width: 200,
      cellClassName: "name-column--cell",
    },
    {
      field: "last_name",
      headerName: "Last Name",
    },
    { field: "email", headerName: "Email", width: 200 },
    { field: "route_date", headerName: "Route Date", width: 200 },
    {
      field: "view_route",
      headerName: "View Route",
      width: 100,
      renderCell: ({ row }) => {
        return (
          <LoadingButton
            variant="contained"
            loading={isMapLoading}
            onClick={async () => {
              const res = await getRouteByDateAndUser({
                user: row.user_id,
                route_date: row.route_date,
              });
              console.log("response view", res);
              if (res?.error?.status == "PARSING_ERROR") {
                setRouteMap(res?.error?.data);
                setRowData(row);
                setOpenMap(true);
                return;
              }
              if (res?.error) {
                toast.error(res?.error?.data?.message);
              }
            }}
          >
            View
          </LoadingButton>
        );
      },
    },
  ];
  return (
    <Box m="20px">
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Header title="All Routes" />
      </Box>
      <Box
        m="8px 0 0 0"
        height="80vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300],
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none",
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiCheckbox-root": {
            color: `${colors.greenAccent[200]} !important`,
          },
        }}
      >
        {isFetching ? (
          <Box display="flex" alignItems="center" justifyContent="center">
            <CircularProgress />
          </Box>
        ) : (
          routeList?.length > 0 && (
            <DataGrid
              pagination={false}
              hideFooter={true}
              rows={routeList}
              columns={columns}
              p
            />
          )
        )}
      </Box>
      <CustomDialogForMap
        open={openMap}
        setOpen={setOpenMap}
        isMapLoading={isMapLoading}
        routeMap={routeMap}
        rowData={rowData}
      />
    </Box>
  );
};

export default AllRoutes;

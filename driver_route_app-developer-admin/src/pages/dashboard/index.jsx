import {
  Box,
  Button,
  IconButton,
  Typography,
  useTheme,
  useMediaQuery,
  Stack,
  CircularProgress,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { useSelector } from "react-redux";

import { tokens } from "../../theme";
import Header from "../../components/Header";
import { useMemo, useState } from "react";
import { LoadingButton } from "@mui/lab";
import { Formik } from "formik";
import CustomDropdown from "../../components/common/input/CustomDropdown";
import createRoutePlanValidationSchema from "../../utils/formValidations/createRoutePlan.validations";
import CustomDatePicker from "../../components/common/input/CustomDatePicker";
import CustomFileUploader from "../../components/common/input/CustomFileUploader";
import dayjs from "dayjs";
import { useGetAllDriversQuery } from "../../redux/services/driver/driver.service";
import { useCreateRouteMutation } from "../../redux/services/route/route.service";
import { toast } from "react-hot-toast";

const Dashboard = () => {
  const theme = useTheme();
  const smScreen = useMediaQuery(theme.breakpoints.up("sm"));
  const colors = tokens(theme.palette.mode);
  const [routeMap, setRouteMap] = useState(null);
  const [createRoute, { isLoading: isCreatingRoute }] =
    useCreateRouteMutation();

  const { data, isLoading } = useGetAllDriversQuery();
  const driverOptions = useMemo(() => {
    return data?.data?.map((driver) => {
      return {
        value: driver.id,
        label: driver.first_name + " " + driver.last_name,
      };
    });
  }, [data]);

  const handleRouteCreation = async (values) => {
    const formData = new FormData();
    formData.append("user", values.user);
    formData.append(
      "route_date",
      dayjs(values.route_date).format("YYYY-MM-DD")
    );
    formData.append("upload", values.upload);
    const res = await createRoute(formData);
    if (res?.error?.status == "PARSING_ERROR") {
      setRouteMap(res?.error?.data);
      return;
    }
    if (typeof res?.error?.data?.errors == "object") {
      toast.error(Object.values(res?.error?.data?.errors)[0][0]);
      return;
    }
    if (res?.error?.data?.message) {
      toast.error(res?.error?.data?.message);
      return;
    }
    toast.error("Unable to create route for this CSV");
  };

  return (
    <Box m="20px">
      {/* HEADER */}

      <Box
        display={smScreen ? "flex" : "block"}
        flexDirection={smScreen ? "row" : "column"}
        justifyContent={smScreen ? "space-between" : "start"}
        alignItems={smScreen ? "center" : "start"}
        m="10px 0"
      >
        <Header
          title="Route Management"
          subtitle="Welcome to the Admin Dashboard"
        />
      </Box>

      {/* GRID & CHARTS */}
      <Grid container rowSpacing={1}>
        <Grid
          xs={12}
          sm={12}
          md={11.8}
          lg={11.8}
          container
          rowSpacing={1}
          columnSpacing={{ xs: 1, sm: 2, md: 3 }}
        >
          <Grid xs={12}>
            <Box backgroundColor={colors.primary[400]}>
              <Box
                mt="25px"
                p="30px"
                display="flex"
                justifyContent="space-between"
                alignItems="center"
              >
                <Box>
                  <Typography
                    variant="h3"
                    fontWeight="600"
                    color={colors.grey[100]}
                  >
                    Create a Route Plan
                  </Typography>
                </Box>
              </Box>
              <Box display="flex" alignItems="center" justifyContent="center">
                {driverOptions && driverOptions.length > 0 ? (
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <Formik
                      initialValues={{
                        user: 0,
                        route_date: dayjs(Date.now()),
                        upload: null,
                      }}
                      validationSchema={createRoutePlanValidationSchema}
                      onSubmit={handleRouteCreation}
                    >
                      {(props) => {
                        const {
                          values,
                          isSubmitting,
                          handleChange,
                          handleSubmit,
                        } = props;
                        return (
                          <form
                            onSubmit={handleSubmit}
                            style={{
                              width: "90%",
                              paddingBottom: "30px",
                            }}
                          >
                            <Stack direction="column">
                              <CustomDropdown
                                name="user"
                                label="Choose Driver"
                                sublabel="Driver"
                                options={driverOptions}
                              />
                              <CustomDatePicker
                                name="route_date"
                                label="Choose Date"
                              />
                              <CustomFileUploader
                                name="upload"
                                label="Upload Csv"
                              />
                              <LoadingButton
                                loading={isCreatingRoute}
                                variant="contained"
                                type="submit"
                              >
                                Create Route
                              </LoadingButton>
                            </Stack>
                          </form>
                        );
                      }}
                    </Formik>
                  </LocalizationProvider>
                ) : driverOptions?.length == 0 ? (
                  <Typography variant="body1">
                    There are currently no active drivers
                  </Typography>
                ) : (
                  <CircularProgress />
                )}
              </Box>
            </Box>
          </Grid>
          {routeMap && (
            <Grid xs={12}>
              <Box backgroundColor={colors.primary[400]} padding="30px">
                <iframe
                  width="100%"
                  height="500px"
                  sandbox="allow-scripts"
                  srcDoc={routeMap}
                />
              </Box>
            </Grid>
          )}
        </Grid>
      </Grid>
    </Box>
  );
};

export default Dashboard;

import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseQueryWithTokenAndLogout } from "../customBaseQuery";

const routeService = createApi({
  reducerPath: "route_service",
  baseQuery: baseQueryWithTokenAndLogout,
  endpoints: (builder) => ({
    createRoute: builder.mutation({
      query: (body) => ({
        url: "add_route",
        method: "POST",
        body,
      }),
    }),
    getRouteByDateAndUser: builder.mutation({
      query: (body) => ({
        url: "view_route",
        method: "POST",
        body,
      }),
      keepUnusedDataFor: 0,
    }),
    getAllDriversWithRoutes: builder.query({
      query: () => ({
        url: "all_routes",
        method: "GET",
      }),
    }),
  }),
});

export default routeService;
export const {
  useCreateRouteMutation,
  useGetRouteByDateAndUserMutation,
  useGetAllDriversWithRoutesQuery,
} = routeService;

import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseQueryWithTokenAndLogout } from "../customBaseQuery";

const driverService = createApi({
  reducerPath: "driver_service",
  baseQuery: baseQueryWithTokenAndLogout,
  endpoints: (builder) => ({
    getAllDrivers: builder.query({
      query: () => ({
        url: "all_users",
        method: "GET",
      }),
    }),
  }),
});

export default driverService;
export const { useGetAllDriversQuery } = driverService;

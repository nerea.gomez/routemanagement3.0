import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { queryWithBaseUrl } from "../customBaseQuery";

const authService = createApi({
  reducerPath: "auth_service",
  baseQuery: queryWithBaseUrl,
  endpoints: (builder) => ({
    login: builder.mutation({
      query: (body) => ({
        url: "admin_login",
        method: "POST",
        body: body,
      }),
    }),
  }),
});

export default authService;
export const { useLoginMutation } = authService;

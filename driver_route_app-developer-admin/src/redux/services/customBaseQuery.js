import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const queryWithBaseUrl = fetchBaseQuery({
  baseUrl: process.env.REACT_APP_BASE_URL, // attach bearer token to headers in every request
  prepareHeaders: (headers, { getState }) => {
    const token = getState().authSlice.token;
    if (token) {
      headers.set("Authorization", `Bearer ${token}`);
    }
    return headers;
  },
});

// redirect user to login page when token is expired
export const baseQueryWithTokenAndLogout = async (args, api, extraOptions) => {
  try {
    var result = await queryWithBaseUrl(args, api, extraOptions);
    if (result.error && result.error.status === 401) {
      localStorage.clear();
      window.location.href = "/login";
    }
    return result;
  } catch (error) {
    console.log("err", error);
  }
};

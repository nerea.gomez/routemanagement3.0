import { configureStore, combineReducers } from "@reduxjs/toolkit";
import persistReducer from "redux-persist/es/persistReducer";
import persistStore from "redux-persist/es/persistStore";
import storage from "redux-persist/lib/storage";
import {
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
} from "redux-persist";
import authSlice from "../slices/auth/auth.slice";
import authService from "../services/auth/auth.service";
import driverService from "../services/driver/driver.service";
import routeService from "../services/route/route.service";

const reducers = combineReducers({
  authSlice,
  [authService.reducerPath]: authService.reducer,
  [driverService.reducerPath]: driverService.reducer,
  [routeService.reducerPath]: routeService.reducer,
});
const persistConfig = {
  key: "root",
  version: 1,
  storage,
  whitelist: ["authSlice"],
};
const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat([
      authService.middleware,
      driverService.middleware,
      routeService.middleware,
    ]),
});
export let persistore = persistStore(store);

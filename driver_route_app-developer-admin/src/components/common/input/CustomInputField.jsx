import React, { useEffect, useState } from "react";
import FormControl from "@mui/material/FormControl";
import { useField } from "formik";
import { IconButton, TextField, Typography } from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";

const CustomInputField = ({
  label,
  sublabel,
  sensitive,
  options,
  ...props
}) => {
  const [field, meta] = useField(props);
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);

  return (
    <FormControl fullWidth>
      {label && (
        <Typography variant="h6" id="demo-simple-select-label">
          {label}
        </Typography>
      )}
      <TextField
        {...props}
        {...field}
        type={sensitive ? (showPassword ? "text" : "password") : "text"}
        InputProps={{
          ...(sensitive && {
            endAdornment: (
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                edge="end"
              >
                {showPassword ? (
                  <VisibilityOff opacity={0.8} />
                ) : (
                  <Visibility opacity={0.8} />
                )}
              </IconButton>
            ),
          }),
        }}
      />
      {meta.touched && meta.error && (
        <Typography variant="h6" color="red">
          {meta.error}
        </Typography>
      )}
    </FormControl>
  );
};

export default CustomInputField;

import React, { useRef, useState } from "react";
import { useField, useFormikContext } from "formik";
import { Box, IconButton, Stack, Typography } from "@mui/material";
import UploadIcon from "@mui/icons-material/Upload";
import { tokens } from "../../../theme";
import { useTheme } from "@mui/material";
import CancelIcon from "@mui/icons-material/Cancel";

const CustomFileUploader = ({
  label,
  uploadText = "Click to Upload CSV ",
  ...props
}) => {
  const [field, meta] = useField(props);
  const theme = useTheme();

  const colors = tokens(theme.palette.mode);
  const ref = useRef();
  const { setFieldValue, errors } = useFormikContext();
  const [fileName, setFileName] = useState("");
  const [file, setFile] = useState(null);
  // Triggers
  const handleClick = () => {
    ref.current.click();
  };

  const handleChange = (e) => {
    setFile(e.target.files[0]);
    setFieldValue(props.name, e.target.files[0]);
    setFileName(e.target.files[0].name);
    e.target.value = null;
  };
  return (
    <Box my={2}>
      {label && (
        <Typography variant="h6" id="demo-simple-select-label">
          {label}
        </Typography>
      )}
      <Box
        onClick={handleClick}
        sx={{
          cursor: "pointer",
          border: `1px dashed ${colors.grey[100]}`,
          height: 70,
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          "&:hover": {
            border: `1px solid ${colors.grey[500]}`,
          },
        }}
      >
        {file ? (
          <Stack alignItems={"center"} spacing={1} direction="row">
            <Typography
              variant="h6"
              color={meta.error?.startsWith("File") ? "red" : "green"}
            >
              {fileName}
            </Typography>
            <IconButton
              sx={{ color: colors.redAccent[500] }}
              onClick={(e) => {
                e.stopPropagation();
                setFile(null);
                setFieldValue(props.name, null);
                setFileName("");
              }}
            >
              <CancelIcon />
            </IconButton>
          </Stack>
        ) : (
          <Stack alignItems={"center"} spacing={1} direction="row">
            <UploadIcon
              sx={{ fontSize: 30, color: colors.grey[100], opacity: 0.5 }}
            />
            <Typography variant="subtitle1">{uploadText}</Typography>
          </Stack>
        )}
      </Box>
      <input
        type="file"
        accept=".csv"
        ref={ref}
        hidden
        width={"100%"}
        height="100%"
        onChange={handleChange}
      />
      {meta.touched && meta.error && (
        <Typography variant="h6" color="red">
          {meta.error}
        </Typography>
      )}
    </Box>
  );
};

export default CustomFileUploader;

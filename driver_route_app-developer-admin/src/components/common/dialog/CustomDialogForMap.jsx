import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import CloseIcon from "@mui/icons-material/Close";
import Slide from "@mui/material/Slide";
import { Box, CircularProgress, useMediaQuery } from "@mui/material";
import { useTheme } from "@mui/material";
import { tokens } from "../../../theme";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function CustomDialogForMap({
  open,
  setOpen,
  isMapLoading,
  routeMap,
  rowData,
}) {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const isSm = useMediaQuery("(max-width:770px)");

  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar sx={{ position: "relative", paddingX: isSm ? 0 : "100px" }}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
              Route Map for{" "}
              <b>{rowData?.first_name + " " + rowData?.last_name}</b> on{" "}
              <b>{rowData?.route_date}</b>
            </Typography>
          </Toolbar>
        </AppBar>
        <Box
          sx={{ paddingLeft: isSm ? 0 : "100px" }}
          backgroundColor={colors.primary[400]}
          // padding="30px"
        >
          {isMapLoading ? (
            <CircularProgress />
          ) : (
            routeMap && (
              <iframe
                width="100%"
                height="500px"
                // height="500px"
                sandbox="allow-scripts"
                srcDoc={routeMap}
              />
            )
          )}
        </Box>
      </Dialog>
    </div>
  );
}

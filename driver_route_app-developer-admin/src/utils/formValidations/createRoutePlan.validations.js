import * as Yup from "yup";

const createRoutePlanValidationSchema = Yup.object().shape({
  user: Yup.string()
    .required("Please choose a driver")
    .not(["0"], "Please choose a driver"),
  route_date: Yup.date().required("Please choose a date"),
  upload: Yup.mixed()
    .required("Please upload a csv file")
    .test(
      "fileSize",
      "File size should be less than 1MB",
      (value) => value?.size / (1024 * 1024) < 1
    ),
});

export default createRoutePlanValidationSchema;

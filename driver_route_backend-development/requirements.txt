asgiref==3.6.0
branca==0.6.0
certifi==2023.5.7
charset-normalizer==3.1.0
Django==3.2.12
django-cors-headers==3.10.1
djangorestframework==3.13.1
djangorestframework-simplejwt==4.3.0
folium==0.14.0
idna==3.4
Jinja2==3.1.2
MarkupSafe==2.1.2
numpy==1.24.3
openrouteservice==2.3.3
psycopg2-binary==2.9.3
PyJWT==1.7.1
pytz==2023.3
requests==2.30.0
sqlparse==0.4.4
urllib3==2.0.2
googlemaps==4.10.0
pandas==2.0.1
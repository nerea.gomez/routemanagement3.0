import django.contrib.auth.password_validation as validators
from django.contrib.auth import authenticate
from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from .models import *
from rest_framework import serializers


def file_validator(file):
    max_file_size = 1024 * 1024 * 1  # 1MB
    if file.size > max_file_size:
        raise serializers.ValidationError(_('Max file size is 1MB'))
class SignupSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    email = serializers.EmailField(required=True)
    password = serializers.CharField(max_length=128, label='Password', style={'input_type': 'password'},
                                     write_only=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'password']

    @staticmethod
    def validate_password(data):
        validators.validate_password(password=data, user=User)
        return data

    def validate(self, data):
        if User.objects.filter(email=data['email'].lower()).exists():
            raise serializers.ValidationError({'email': _('Email already exists')})
        return data

class SigninSerializer(serializers.ModelSerializer):
    email = serializers.CharField(required=False)
    password = serializers.CharField(
        max_length=128,
        label='Password',
        style={'input_type': 'password'},
        write_only=True,
        required=True
    )

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'password']

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        if User.objects.filter(email=email.lower(), is_active=False).exists():
            raise serializers.ValidationError({'error': _('Account disabled, contact admin')})
        if User.objects.filter(username=email).exists():
            user_email = User.objects.filter(username=email).first().email
            user = authenticate(email=user_email.lower(), password=password)
        else:
            user = authenticate(email=email.lower(), password=password)
        if not user:
            raise serializers.ValidationError({'error': _('Invalid credentials')})
        return user

class AdminLoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(
        max_length=128,
        label='Password',
        style={'input_type': 'password'},
        write_only=True,
        required=True
    )

    class Meta:
        model = User
        fields = ('email', 'password')

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        if email and password:
            user = authenticate(email=email.lower(), password=password)
            if user:
                if user.is_active:
                    if user.is_superuser:
                        attrs['user'] = user
                    else:
                        raise serializers.ValidationError({'error': _('User is not admin')})
                else:
                    raise serializers.ValidationError({'error': _('User is not active')})
            else:
                raise serializers.ValidationError({'error': _('Invalid credentials')})
        else:
            raise serializers.ValidationError({'error': _('Must provide email and password')})
        return attrs

class AllUsersSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name']


class AddRouteSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    user = serializers.SlugRelatedField(queryset=User.objects.all(), slug_field='id', required=True)
    upload = serializers.FileField(validators=[file_validator],required=True)
    route_date = serializers.DateField(required=True)

    class Meta:
        model = DriverRoute
        fields = ['id', 'user', 'upload', 'route_date']

class ViewRouteSerializer(serializers.Serializer):
    route_date = serializers.DateField(required=True)

class AllRoutesSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['route_date'] = DriverRoute.objects.filter(user=instance).exclude(Q(driver_data='') | Q(driver_data__isnull=True)).values_list("route_date", flat=True)
        return data

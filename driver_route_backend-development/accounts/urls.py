from rest_framework.routers import SimpleRouter
from .views import *
from django.urls import path, include

router = SimpleRouter(trailing_slash=False)
router.register("api/user", UserViewSet, basename="signup")
urlpatterns = [
    path("", include(router.urls)),
    path("api/signup", UserViewSet.as_view({"post": "signup"}), name="signup"),
    path("api/login", UserViewSet.as_view({"post": "login"}), name="login"),
    path("api/admin_login", UserViewSet.as_view({"post": "admin_login"}), name="admin_login"),
    path("api/all_users", UserViewSet.as_view({"get": "all_users"}), name="all_users"),
    path("api/add_route", UserViewSet.as_view({"post": "add_route"}), name="add_route"),
    path("api/view_route", UserViewSet.as_view({"post": "view_route"}), name="view_route"),
    path("api/user_routes", UserViewSet.as_view({"get": "user_routes"}), name="user_routes"),
    path("api/all_routes", UserViewSet.as_view({"get": "all_routes"}), name="all_routes"),
]

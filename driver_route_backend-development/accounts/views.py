import json
import ast
import googlemaps
import requests
from django.contrib.auth.hashers import make_password
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken
from route_optimiser.settings import API_KEY, GOOGLE_DIRECTION_URL, GOOGLE_MAPS_GEOCODE
from .serializers import *
import pandas as pd
import time
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def signup(self, request, *args, **kwargs):
        try:
            serializer = SignupSerializer(data=request.data)
            try:
                serializer.is_valid(raise_exception=True)
            except Exception as e:
                error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                         "errors": e.args[0]}
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
            email = request.data.get("email")
            password = request.data.get("password")
            serializer.save(email=email.lower(), password=make_password(password))
            user = User.objects.get(email=email)
            response = {"statusCode": 201, "error": False, "message": "User Registered Successfully",
                        "data": serializer.data, "access": str(AccessToken.for_user(user)),
                        "refresh": str(RefreshToken.for_user(user)),}
            return Response(response, status=status.HTTP_201_CREATED)
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

    def login(self, request, *args, **kwargs):
        try:
            serializer = SigninSerializer(data=request.data)
            try:
                serializer.is_valid(raise_exception=True)
            except Exception as e:
                error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                         "errors": e.args[0]}
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
            user_data = serializer.data
            user = User.objects.get(email=user_data.get("email"))
            return Response(
                data={
                    "statusCode": 200,
                    "error": False,
                    "message": "User logged in successfully",
                    "data": {
                        "user": serializer.data,
                        "access": str(AccessToken.for_user(user)),
                        "refresh": str(RefreshToken.for_user(user)),
                    }
                },
                status=status.HTTP_200_OK
            )
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

    def admin_login(self, request, *args, **kwargs):
        serializer = AdminLoginSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please   check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        user = serializer.validated_data['user']
        data = AllUsersSerializer(user).data
        data['access'] = str(AccessToken.for_user(user))
        data['refresh'] = str(RefreshToken.for_user(user))
        response = {"statusCode": 200, "error": False, "message": "Admin Login successfully", "data": data}
        return Response(response, status=status.HTTP_200_OK)

    def all_users(self, request, *args, **kwargs):
        try:
            user = User.objects.exclude(is_superuser=True)
            user_serializer = AllUsersSerializer(user, many=True)
            response = {"statusCode": 200, "error": False, "message": "User List", "data": user_serializer.data, "count": user.count()}
            return Response(data=response, status=status.HTTP_200_OK)
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

    def add_route(self, request, *args, **kwargs):
        serializer = AddRouteSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        route_date = request.data.get("route_date")
        user = request.data.get("user")
        if not DriverRoute.objects.filter(user=user, route_date=route_date).exists():
            serializer.save()
        else:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Route already exists in this date",
                     }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        route_obj = DriverRoute.objects.filter(user=user, route_date=route_date)
        if route_obj:
            obj = route_obj.last()
            input_file = obj.upload.path
            locations_data = []
            with open(input_file, 'r') as f:
                file_extension = input_file[-5:].split(".")[1]
                if file_extension == 'csv':
                    try:
                        df = pd.read_csv(input_file, delimiter=";", encoding='utf-8')
                    except Exception as e:
                        if route_obj:
                            route_obj.delete()
                        error = {"statusCode": 400, "error": True, "data": "",
                                 "message": "Your provided file not valid",
                                 "errors": "Your provided file not valid"}
                        return Response(error, status=status.HTTP_400_BAD_REQUEST)
                    all_columns = df.columns.tolist()
                if 'Direccion' in all_columns:
                    locations_data.append(df['Direccion'].tolist())
                else:
                    if route_obj:
                        route_obj.delete()
                    error = {"statusCode": 400, "error": True, "data": "", "message": "Direccion not found in file",
                                 }
                    return Response(error, status=status.HTTP_400_BAD_REQUEST)
            try:
                locations = locations_data[0]
                add = []
                complete_addresses = []
                for i in range(0, len(locations)):
                    url = GOOGLE_MAPS_GEOCODE+locations[i]+"&key="+API_KEY
                    payload = ""
                    headers = {
                        'Content-Type': 'application/json'
                    }
                    response = requests.request("POST", url, headers=headers, data=payload)
                    geocode_data = response.json()
                    if geocode_data["status"] == "OK":
                        results = geocode_data["results"]
                        for result in results:
                            address = result["formatted_address"]
                            add.append(address)
                    else:
                        if route_obj:
                            route_obj.delete()
                        message = geocode_data.get("error_message")
                        error = {"statusCode": 400, "error": True, "data": "", "message": message,
                                 }
                        return Response(error, status=status.HTTP_400_BAD_REQUEST)
                gmaps = googlemaps.Client(key=API_KEY)
                origin = add[0]
                complete_addresses.append(origin)
                for i in range(1, len(add)):
                    my_dist = gmaps.distance_matrix(origin, add[i], mode='driving')['rows'][0]['elements'][0]
                    if "duration" in my_dist and "distance" in my_dist:
                        if my_dist.get('distance').get('value') < 2000000:
                            complete_addresses.append(add[i])
                DriverRoute.objects.filter(user=user, route_date=route_date).update(driver_data=complete_addresses)
                chunk_size = 15
                route_starts = []
                count = 0
                waypoints_data = []
                for i in range(0, len(complete_addresses), chunk_size):
                    chunk = complete_addresses[i:i + chunk_size]
                    origin = chunk[0]
                    destination = chunk[-1]
                    separator = "|"
                    waypoints = separator.join(chunk)
                    url = GOOGLE_DIRECTION_URL + origin + "&destination=" + destination + \
                          "&waypoints=optimize:true|" + waypoints + "&key=" + API_KEY
                    payload = {}
                    headers = {}
                    response = requests.request("GET", url, headers=headers, data=payload)
                    directions_data = json.loads(response.text)
                    if directions_data.get("status") != "ZERO_RESULTS":
                        if count == 0:
                            route_starts.append(directions_data.get("routes")[0]['legs'][0])
                            count = 1
                        for leg in directions_data.get("routes")[0]['legs'][0:-1]:
                            data = leg['end_location']
                            data['name'] = leg['end_address']
                            waypoints_data.append(data)
                time.sleep(5)
                context = {
                    'route': route_starts[0],
                    'waypoints': waypoints_data
                }
                return render(request, 'route.html', context)
            except Exception as e:
                if route_obj:
                    route_obj.delete()
                error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                         "errors": e.args[0]}
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
        else:
            error = {"statusCode": 400, "error": True, "data": "", "message": "rout not created",
                     }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)


    def view_route(self, request, *args, **kwargs):
        serializer = ViewRouteSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        rout_date = request.data.get("route_date")
        user = request.data.get("user")
        try:
            if user:
                route_obj = DriverRoute.objects.filter(user_id=int(user), route_date=rout_date)
            else:
                route_obj = DriverRoute.objects.filter(user=request.user, route_date=rout_date)
            if route_obj:
                obj = route_obj.last()
                address = obj.driver_data
                locations = ast.literal_eval(address)
                chunk_size = 15
                route_starts = []
                count = 0
                waypoints_data = []
                for i in range(0, len(locations), chunk_size):
                    chunk = locations[i:i + chunk_size]
                    origin = chunk[0]
                    destination = chunk[-1]
                    separator = "|"
                    waypoints = separator.join(chunk)
                    url = GOOGLE_DIRECTION_URL + origin + "&destination=" + destination + \
                          "&waypoints=optimize:true|" + waypoints + "&key=" + API_KEY
                    payload = {}
                    headers = {}
                    response = requests.request("GET", url, headers=headers, data=payload)
                    directions_data = json.loads(response.text)
                    if directions_data.get("status") != "ZERO_RESULTS" and directions_data.get("status") != "REQUEST_DENIED":
                        if count == 0:
                            route_starts.append(directions_data.get("routes")[0]['legs'][0])
                            count = 1
                        for leg in directions_data.get("routes")[0]['legs'][0:-1]:
                            data = leg['end_location']
                            data['name'] = leg['end_address']
                            waypoints_data.append(data)
                    else:
                        message = directions_data.get("error_message")
                        error = {"statusCode": 400, "error": True, "data": "", "message": message,
                                 }
                        return Response(error, status=status.HTTP_400_BAD_REQUEST)
                time.sleep(5)
                context = {
                    'route': route_starts[0],
                    'waypoints': waypoints_data
                }
                return render(request, 'route.html', context)
            else:
                response = {"statusCode": 200, "error": False, "message": "No added route"}
                return Response(response, status=status.HTTP_200_OK)
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

    def user_routes(self, request, *args, **kwargs):
        try:
            user_routes_list = DriverRoute.objects.filter(user=request.user).exclude(Q(driver_data='') | Q(driver_data__isnull=True)).values_list("route_date", flat=True)
            response = {"statusCode": 200, "error": False, "message": "User List", "data": user_routes_list}
            return Response(data=response, status=status.HTTP_200_OK)
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

    def all_routes(self, request, *args, **kwargs):
        try:
            user = User.objects.exclude(is_superuser=True)
            user_serializer = AllRoutesSerializer(user, many=True)
            response = {"statusCode": 200, "error": False, "message": "All User Routes List", "data": user_serializer.data}
            return Response(data=response, status=status.HTTP_200_OK)
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)


def get_permissions(self):
        if self.action in ['all_users'] or self.action in ['add_route'] or self.action in ['all_routes']:
            permission_classes = [IsAdminUser]
        elif self.action in ['view_route'] or self.action in ['user_routes']:
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]

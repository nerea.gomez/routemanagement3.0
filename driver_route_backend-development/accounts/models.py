from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    username = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(_('email address'), null=True, blank=True, unique=True)
    password = models.CharField(_('password'), max_length=128, null=True, blank=True)
    first_name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.email

class DriverRoute(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_in_driver_rout')
    upload = models.FileField(upload_to='driver_routes_csv_files/')
    route_date = models.DateField(null=True, blank=True)
    template_name = models.CharField(max_length=100, null=True, blank=True)
    driver_data = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

import React, { useEffect } from "react";
import { Box, Divider, Grid, Typography } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import { Formik } from "formik";
import * as Yup from "yup";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import CustomInputField from "../../components/common/input/CustomInputField";
import { useLoginMutation } from "../../redux/services/auth/auth.service";
import { login } from "../../redux/slices/auth/auth.slice";
import toast from "react-hot-toast";
function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const token = useSelector((state) => state.authSlice.token);
  const [loginRequest, { isLoading }] = useLoginMutation();

  const loginRequestHandler = async (userData) => {
    const res = await loginRequest(userData);
    if (res?.error) {
      return toast.error(Object.values(res?.error?.data?.errors)?.[0]?.[0]);
    }
    if (res?.data) {
      dispatch(
        login({
          token: res?.data?.data?.access,
        })
      );
      navigate("/");
    }
  };
  useEffect(() => {
    if (token) {
      navigate("/");
    }
  }, [pathname]);
  return (
    <Box
      sx={{
        height: "100vh",
      }}
    >
      <Grid
        container
        sx={{
          width: "100%",
          height: "100%",
        }}
      >
        <Grid
          item
          xs
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Formik
            initialValues={{
              email: "",
              password: "",
            }}
            onSubmit={async (values, { setSubmitting }) => {
              loginRequestHandler(values);
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string()
                .email("Email should be a valid email")
                .required("Email is required"),
              password: Yup.string().required("Password is required"),
            })}
          >
            {(props) => {
              const { values, isSubmitting, handleChange, handleSubmit } =
                props;
              return (
                <Box width={{ xs: "90%", md: "50%" }}>
                  <form onSubmit={handleSubmit} style={{ width: "100%" }}>
                    <Box sx={{ marginBottom: "50px" }}>
                      <Divider
                        orientation="horizontal"
                        sx={{
                          // marginTop: "100px",
                          border: "4px solid #E8505B",
                          width: "183px",
                          borderRadius: "40px",
                        }}
                      />
                      <Box>
                        <Typography variant="h2">
                          Welcome to Route Management <br />
                          Driver Side
                        </Typography>
                      </Box>
                    </Box>
                    <Box
                      display="flex"
                      flexDirection="column"
                      rowGap={3}
                      sx={{ width: "100%" }}
                    >
                      <CustomInputField
                        name="email"
                        // label="Enter Your Email"
                        placeholder="Enter email"
                      />
                      <CustomInputField
                        name="password"
                        sensitive={true}
                        // label="Password"
                        placeholder="Enter password"
                        // onCutHandler={(e) => e.preventDefault()}
                        // onCopyHandler={(e) => e.preventDefault()}
                        // onPasteHandler={(e) => e.preventDefault()}
                      />
                    </Box>
                    <Typography marginTop={1} textAlign="end" variant="body1">
                      Don't have an account yet?{" "}
                      <Link to="/signup">Sign up</Link>
                    </Typography>
                    <Box display="flex" flexDirection="column">
                      <LoadingButton
                        variant="contained"
                        color="primary"
                        type="submit"
                        loading={isLoading}
                        sx={{
                          mt: 3,
                        }}
                      >
                        <Typography variant="h4">Login</Typography>
                      </LoadingButton>
                    </Box>
                    {/* <Box
                      sx={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center",
                        marginTop: "18px",
                      }}
                    >
                      <Typography
                        variant="body1"
                        onClick={() => navigate("/forget-password")}
                        sx={{ cursor: "pointer" }}
                      >
                        Forget your password?
                      </Typography>
                      <Typography
                        variant="h6"
                        mt="8px"
                        color="secondary.lightRed"
                      >
                        Get helped signed In.
                      </Typography>
                    </Box> */}
                  </form>
                </Box>
              );
            }}
          </Formik>
        </Grid>
      </Grid>
      {/* <Box sx={{ width: "100%" }}>
          <img src={LoginBottomImage} style={{ width: "100%" }} />
        </Box> */}
    </Box>
  );
}

export default Login;

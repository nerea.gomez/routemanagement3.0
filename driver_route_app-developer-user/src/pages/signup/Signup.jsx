import React, { useEffect } from "react";
import { Box, Divider, Grid, Typography } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import { Formik } from "formik";
import * as Yup from "yup";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import CustomInputField from "../../components/common/input/CustomInputField";
import { useSignupMutation } from "../../redux/services/auth/auth.service";
import { login } from "../../redux/slices/auth/auth.slice";
import toast from "react-hot-toast";
function Signup() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const token = useSelector((state) => state.authSlice.token);
  const [signupRequest, { isLoading }] = useSignupMutation();

  const signupRequestHandler = async (userData) => {
    const res = await signupRequest(userData);
    console.log(res?.data?.access);
    if (res?.error) {
      return toast.error(Object.values(res?.error?.data?.errors)?.[0]?.[0]);
    }
    if (res?.data) {
      dispatch(
        login({
          token: res?.data?.access,
        })
      );
      navigate("/");
    }
  };
  useEffect(() => {
    if (token) {
      navigate("/");
    }
  }, [pathname]);
  return (
    <Box
      sx={{
        height: "100vh",
      }}
    >
      <Grid
        container
        sx={{
          width: "100%",
          height: "100%",
        }}
      >
        <Grid
          item
          xs
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Formik
            initialValues={{
              first_name: "",
              last_name: "",
              email: "",
              password: "",
              confirm_password: "",
            }}
            onSubmit={async (values, { setSubmitting }) => {
              signupRequestHandler(values);
            }}
            validationSchema={Yup.object().shape({
              first_name: Yup.string().required("First name is required"),
              last_name: Yup.string().required("Last name is required"),
              email: Yup.string()
                .email("Email should be a valid email")
                .required("Email is required"),
              password: Yup.string()
                .required("Password is required")
                .min(8, "Password should have a minimum of 8 characters")
                .matches(
                  /(?=[A-Za-z0-9@#$%^&+!=]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+!=])(?=.{8,}).*$/,
                  "Must contain atleast one lowercase, one uppercase, a number, and a symbol"
                ),
              confirm_password: Yup.string()
                .required("Confirm Password is required")
                .oneOf([Yup.ref("password"), null], "Password must match"),
            })}
          >
            {(props) => {
              const { values, isSubmitting, handleChange, handleSubmit } =
                props;
              return (
                <Box width={{ xs: "90%", md: "50%" }}>
                  <form onSubmit={handleSubmit} style={{ width: "100%" }}>
                    <Box sx={{ marginBottom: "50px" }}>
                      <Divider
                        orientation="horizontal"
                        sx={{
                          border: "4px solid #E8505B",
                          width: "183px",
                          borderRadius: "40px",
                        }}
                      />
                      <Box>
                        <Typography variant="h2">
                          Welcome to Route Management <br />
                          Sign up as Driver
                        </Typography>
                      </Box>
                    </Box>
                    <Box
                      display="flex"
                      flexDirection="column"
                      rowGap={3}
                      sx={{ width: "100%" }}
                    >
                      <CustomInputField
                        name="first_name"
                        placeholder="Enter first name"
                      />
                      <CustomInputField
                        name="last_name"
                        placeholder="Enter last name"
                      />
                      <CustomInputField
                        name="email"
                        placeholder="Enter email"
                      />
                      <CustomInputField
                        name="password"
                        sensitive={true}
                        placeholder="Enter password"
                      />
                      <CustomInputField
                        name="confirm_password"
                        sensitive={true}
                        placeholder="Confirm Password"
                      />
                    </Box>
                    <Typography marginTop={1} textAlign="end" variant="body1">
                      Already have an account?
                      <Link to="/login">Login</Link>
                    </Typography>
                    <Box display="flex" flexDirection="column">
                      <LoadingButton
                        variant="contained"
                        color="primary"
                        type="submit"
                        loading={isLoading}
                        sx={{
                          mt: 3,
                        }}
                      >
                        <Typography variant="h4">Signup</Typography>
                      </LoadingButton>
                    </Box>
                  </form>
                </Box>
              );
            }}
          </Formik>
        </Grid>
      </Grid>
    </Box>
  );
}

export default Signup;

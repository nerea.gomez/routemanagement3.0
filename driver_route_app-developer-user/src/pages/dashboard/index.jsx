import {
  Box,
  Button,
  IconButton,
  Typography,
  useTheme,
  useMediaQuery,
  Stack,
  CircularProgress,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { useSelector } from "react-redux";

import { tokens } from "../../theme";
import Header from "../../components/Header";
import GeographyChart from "../../components/GeographyChart";
import { useMemo, useState } from "react";
import { LoadingButton } from "@mui/lab";
import { Formik } from "formik";
import CustomDropdown from "../../components/common/input/CustomDropdown";
import createRoutePlanValidationSchema from "../../utils/formValidations/getUserRoutes.validations";
import CustomDatePicker from "../../components/common/input/CustomDatePicker";
import CustomFileUploader from "../../components/common/input/CustomFileUploader";
import dayjs from "dayjs";
import { useGetAllDriversQuery } from "../../redux/services/driver/driver.service";
import {
  useGetRouteByDateMutation,
  useGetRouteDatesQuery,
} from "../../redux/services/route/route.service";
import { toHaveStyle } from "@testing-library/jest-dom/dist/matchers";
import { toast } from "react-hot-toast";

const Dashboard = () => {
  const theme = useTheme();
  const smScreen = useMediaQuery(theme.breakpoints.up("sm"));
  const colors = tokens(theme.palette.mode);
  const [routeMap, setRouteMap] = useState(null);
  const [getRoute, { isLoading: isfetchingRoute }] =
    useGetRouteByDateMutation();
  console.log("is route fetching", isfetchingRoute);
  const { data, isLoading } = useGetRouteDatesQuery();
  const dateOptions = useMemo(() => {
    return data?.data?.map((date) => {
      return {
        value: date,
        label: date,
      };
    });
  }, [data]);

  const handleRouteFetching = async (values) => {
    const res = await getRoute({ route_date: values?.view_route });
    if (res?.error?.status == "PARSING_ERROR") {
      setRouteMap(res?.error?.data);
      return;
    }
    if (res?.error?.data?.message) {
      toast.error(res?.error?.data?.message);
    }
  };

  return (
    <Box m="20px">
      {/* HEADER */}

      <Box
        display={smScreen ? "flex" : "block"}
        flexDirection={smScreen ? "row" : "column"}
        justifyContent={smScreen ? "space-between" : "start"}
        alignItems={smScreen ? "center" : "start"}
        m="10px 0"
      >
        <Header
          title="Route Management"
          subtitle="Welcome to the Driver Dashboard"
        />
      </Box>

      {/* GRID & CHARTS */}
      <Grid container rowSpacing={1}>
        <Grid
          xs={12}
          sm={12}
          md={11.8}
          lg={11.8}
          container
          rowSpacing={1}
          columnSpacing={{ xs: 1, sm: 2, md: 3 }}
        >
          <Grid xs={12}>
            <Box backgroundColor={colors.primary[400]}>
              <Box
                mt="25px"
                p="30px"
                display="flex"
                justifyContent="space-between"
                alignItems="center"
              >
                <Box>
                  <Typography
                    variant="h3"
                    fontWeight="600"
                    color={colors.grey[100]}
                  >
                    See your route plans
                  </Typography>
                </Box>
              </Box>
              <Box display="flex" alignItems="center" justifyContent="center">
                {dateOptions && dateOptions.length > 0 ? (
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <Formik
                      initialValues={{
                        view_route: 0,
                      }}
                      validationSchema={createRoutePlanValidationSchema}
                      onSubmit={handleRouteFetching}
                    >
                      {(props) => {
                        const {
                          values,
                          isSubmitting,
                          handleChange,
                          handleSubmit,
                        } = props;
                        return (
                          <form
                            onSubmit={handleSubmit}
                            style={{
                              width: "90%",
                              paddingBottom: "30px",
                            }}
                          >
                            <Stack direction="column" spacing={2}>
                              <CustomDropdown
                                name="view_route"
                                label="Choose Date"
                                sublabel="Date of the route plan"
                                options={dateOptions}
                              />

                              <LoadingButton
                                loading={isfetchingRoute}
                                variant="contained"
                                type="submit"
                              >
                                Get Route
                              </LoadingButton>
                            </Stack>
                          </form>
                        );
                      }}
                    </Formik>
                  </LocalizationProvider>
                ) : isLoading ? (
                  <CircularProgress />
                ) : (
                  <Typography
                    variant="h6"
                    fontWeight="600"
                    color={colors.grey[100]}
                  >
                    You do not have any active routes
                  </Typography>
                )}
              </Box>
            </Box>
          </Grid>
          {routeMap && (
            <Grid xs={12}>
              <Box backgroundColor={colors.primary[400]} padding="30px">
                {isfetchingRoute ? (
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                  >
                    <CircularProgress />
                  </Box>
                ) : (
                  <iframe
                    width="100%"
                    height="500px"
                    sandbox="allow-scripts"
                    srcDoc={routeMap}
                  />
                )}
              </Box>
            </Grid>
          )}
        </Grid>
      </Grid>
    </Box>
  );
};

export default Dashboard;

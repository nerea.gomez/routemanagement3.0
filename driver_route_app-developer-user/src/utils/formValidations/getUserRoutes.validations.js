import * as Yup from "yup";

const getUserRoutesByDateValidationSchema = Yup.object().shape({
  view_route: Yup.string()
    .required("Please choose a date")
    .not(["0"], "Please choose a date"),
});

export default getUserRoutesByDateValidationSchema;

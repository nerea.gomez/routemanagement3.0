import { ColorModeContext, useMode } from "./theme";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { Routes, Route } from "react-router-dom";
import Dashboard from "./pages/dashboard";
import Login from "./pages/login/Login";
import Protected from "./routing/Protected";
import DashboardLayout from "./layout/DashboardLayout";
import { Toaster } from "react-hot-toast";
import Signup from "./pages/signup/Signup";

const App = () => {
  const [theme, colorMode] = useMode();
  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/" element={<Protected Component={DashboardLayout} />}>
            <Route path="/" element={<Dashboard />} />
          </Route>
        </Routes>
        <Toaster reverseOrder="false" position="top-center" />
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};

export default App;

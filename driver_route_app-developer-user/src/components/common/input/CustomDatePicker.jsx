import { Box, TextField, Typography } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { useField, useFormikContext } from "formik";

const CustomDatePicker = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  const { setFieldValue } = useFormikContext();

  return (
    <Box my={2}>
      {label && (
        <Typography variant="h6" id="demo-simple-select-label">
          {label}
        </Typography>
      )}
      <DatePicker
        {...props}
        slotProps={{ textField: { fullWidth: true } }}
        value={field.value ?? null}
        disablePast
        onChange={(val) => setFieldValue(props.name, val)}
        onKeyDown={(e) => {
          e.preventDefault();
        }}
      />
      {meta.touched && meta.error && (
        <Typography variant="h6" color="red">
          {meta.error}
        </Typography>
      )}
    </Box>
  );
};
export default CustomDatePicker;

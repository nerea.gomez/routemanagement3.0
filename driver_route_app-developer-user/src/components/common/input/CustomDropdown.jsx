import React, { useEffect, useState } from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useField } from "formik";
import { Typography } from "@mui/material";

const CustomDropdown = ({ label, sublabel, options, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <FormControl fullWidth>
      {label && (
        <Typography variant="h6" id="demo-simple-select-label">
          {label}
        </Typography>
      )}
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        {...field}
        {...props}
      >
        <MenuItem value={0} key={0} disabled>
          {"Select Date..."}
        </MenuItem>
        {options.map((item) => (
          <MenuItem value={item.value} key={item.value}>
            {item.label}
          </MenuItem>
        ))}
      </Select>
      {meta.touched && meta.error && (
        <Typography variant="h6" color="red">
          {meta.error}
        </Typography>
      )}
    </FormControl>
  );
};

export default CustomDropdown;

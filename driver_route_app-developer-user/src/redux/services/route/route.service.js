import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseQueryWithTokenAndLogout } from "../customBaseQuery";

const routeService = createApi({
  reducerPath: "route_service",
  tagTypes: ["map"],
  baseQuery: baseQueryWithTokenAndLogout,
  endpoints: (builder) => ({
    getRouteDates: builder.query({
      query: (body) => ({
        url: "user_routes",
        method: "GET",
        body,
      }),
      providesTags: ["map"],
    }),
    getRouteByDate: builder.mutation({
      query: (body) => ({
        url: "view_route",
        method: "POST",
        body,
      }),
      keepUnusedDataFor: 0,
      invalidatesTags: ["map"],
    }),
  }),
});

export default routeService;
export const { useGetRouteDatesQuery, useGetRouteByDateMutation } =
  routeService;

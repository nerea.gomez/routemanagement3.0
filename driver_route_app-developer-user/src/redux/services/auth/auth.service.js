import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { queryWithBaseUrl } from "../customBaseQuery";

const authService = createApi({
  reducerPath: "auth_service",
  baseQuery: queryWithBaseUrl,
  endpoints: (builder) => ({
    login: builder.mutation({
      query: (body) => ({
        url: "login",
        method: "POST",
        body: body,
      }),
    }),
    signup: builder.mutation({
      query: (body) => ({
        url: "signup",
        method: "POST",
        body: body,
      }),
    }),
  }),
});

export default authService;
export const { useLoginMutation, useSignupMutation } = authService;

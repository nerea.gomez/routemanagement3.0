import React from "react";
import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";

const Protected = (props) => {
  const token = useSelector((state) => state.authSlice.token);
  const { Component } = props;
  return <>{token !== null ? <Component /> : <Navigate to="/login" />}</>;
};

export default Protected;
